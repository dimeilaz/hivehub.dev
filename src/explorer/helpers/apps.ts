import { getTransaction as getHiveEngineTransaction } from '~/explorer/api/hiveEngineApi'
import { getTransaction as getSplinterlandsTransaction } from '~/explorer/api/splinterlandsApi'
import { Operation } from '@hiveio/dhive'
import HiveEngineIcon from '~/assets/images/icons/hive-engine.svg'
import SplinterlandsIcon from '~/assets/images/icons/splinterlands.png'
import PeakMonstersIcon from '~/assets/images/icons/peakmonsters_red.svg'
import DluxIcon from '~/assets/images/icons/dlux.svg'
import CryptoBrewMasterIcon from '~/assets/images/icons/cryptobrewmaster.png'
import RabonaIcon from '~/assets/images/icons/rabona.png'
import dCropsIcon from '~/assets/images/icons/dcrops.svg'
import HashkingsIcon from '~/assets/images/icons/hashkings.png'
import PodpingIcon from '~/assets/images/icons/podping.png'
import gLsIcon from '~/assets/images/icons/gls.png'
// import dCityIcon from '~/assets/images/icons/dcity.png'
// import RisingStarIcon from '~/assets/images/icons/risingstar.png'

type AppConfig = {
  name: string
  icon?: string
  match: (operation: Operation) => boolean
  supported: boolean
  load: (trxId: string, operation: any, index: number, totalOps: number) => any
  validate: (obj: any) => ValidationResult
}

type ValidationResult = {
  success: boolean
  error?: string
}

const apps: AppConfig[] = [
  {
    name: 'Hive-Engine',
    icon: HiveEngineIcon,
    match: operation => operation[0] === 'custom_json' && operation[1].id === 'ssc-mainnet-hive',
    supported: true,
    load: async (trxId: string, operation: any, index: number, totalOps: number) => {
      const json = JSON.parse(operation[1].json)
      if (Array.isArray(json) && json.length > 1) {
        return Promise.all(json.map((j, i) => getHiveEngineTransaction(`${trxId}-${index + i}`)))
      } else {
        const indexedTrxId = totalOps > 1 ? `${trxId}-${index}` : trxId
        return getHiveEngineTransaction(indexedTrxId)
      }
    },
    validate: (obj: any) => {
      try {
        if (!obj) {
          return { success: false, error: 'Transaction not found!' }
        }
        if (!obj.logs) {
          return { success: false }
        }

        const logs = JSON.parse(obj.logs)
        return { success: logs.errors ? false : true, error: logs.errors && logs.errors.toString() }
      } catch (error) {
        return { success: false }
      }
    }
  },
  {
    name: 'Splinterlands',
    icon: SplinterlandsIcon,
    match: operation => operation[0] === 'custom_json' && operation[1].id.startsWith('sm_'),
    supported: true,
    load: async (trxId: string) => await getSplinterlandsTransaction(trxId),
    validate: (obj: any) => ({ success: obj.trx_info && obj.trx_info.success, error: obj.trx_info.error })
  },
  {
    name: 'PeakMonsters',
    icon: PeakMonstersIcon,
    match: operation => operation[0] === 'custom_json' && operation[1].id.startsWith('pm_'),
    supported: false,
    load: async (trxId: string) => await getSplinterlandsTransaction(trxId),
    validate: (obj: any) => ({ success: obj.trx_info && obj.trx_info.success, error: obj.trx_info.error })
  },
  {
    name: 'Dlux',
    icon: DluxIcon,
    match: operation => operation[0] === 'custom_json' && operation[1].id.startsWith('dlux_'),
    supported: false,
    load: async (trxId: string) => [],
    validate: (obj: any) => ({ success: true })
  },
  {
    name: 'CryptoBrewMaster',
    icon: CryptoBrewMasterIcon,
    match: operation => operation[0] === 'custom_json' && operation[1].id.startsWith('cbm__'),
    supported: false,
    load: async (trxId: string) => [],
    validate: (obj: any) => ({ success: true })
  },
  {
    name: 'Rabona',
    icon: RabonaIcon,
    match: operation => operation[0] === 'custom_json' && operation[1].id === 'rabona',
    supported: false,
    load: async (trxId: string) => [],
    validate: (obj: any) => ({ success: true })
  },
  {
    name: 'dCrops',
    icon: dCropsIcon,
    match: operation => operation[0] === 'custom_json' && operation[1].id === 'dcrops',
    supported: false,
    load: async (trxId: string) => [],
    validate: (obj: any) => ({ success: true })
  },
  {
    name: 'Hashkings',
    icon: HashkingsIcon,
    match: operation => operation[0] === 'custom_json' && operation[1].id.startsWith('qwoyn_'),
    supported: false,
    load: async (trxId: string) => [],
    validate: (obj: any) => ({ success: true })
  },
  {
    name: 'Podping',
    icon: PodpingIcon,
    match: operation => operation[0] === 'custom_json' && operation[1].id.startsWith('pp_'),
    supported: false,
    load: async (trxId: string) => [],
    validate: (obj: any) => ({ success: true })
  },
  {
    name: 'Genesis League Sports',
    icon: gLsIcon,
    match: operation => operation[0] === 'custom_json' && operation[1].id.startsWith('gls-'),
    supported: false,
    load: async (trxId: string) => [],
    validate: (obj: any) => ({ success: true })
  }
  // {
  //   name: 'dCity',
  //   icon: dCityIcon,
  //   match: operation => operation[0] === 'custom_json' && operation[1].id === 'dcity',
  //   supported: false,
  //   load: async (trxId: string) => [],
  //   validate: (obj: any) => ({ success: true })
  // },
  // {
  //   name: 'Rising Star',
  //   icon: RisingStarIcon,
  //   match: operation => operation[0] === 'custom_json' && operation[1].id === 'risingstar',
  //   supported: false,
  //   load: async (trxId: string) => [],
  //   validate: (obj: any) => ({ success: true })
  // }
]

export default apps
