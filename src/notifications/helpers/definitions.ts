/**
 * Conditions are rendered as follows:
 * .1 Strings are rendered on the same level.
 *    ['a', 'b'] --> both are conditions on the same level
 * .2 An array is rendered at a lower level and the string that precedes it becomes a title
 *    ['a', 'b', ['c', 'd']] --> b is the title for the sub-level containing c and d.
 */
export const availableOperations = {
  core: {
    settings: {
      name: 'Core',
      prefix: '',
      color: '#E31337'
    },
    operations: [
      {
        operation: 'vote',
        conditions: ['voter', 'author', 'permlink', 'weight']
      },
      {
        operation: 'comment',
        conditions: ['parent_author', 'parent_permlink', 'author', 'permlink', 'title', 'body', 'json_metadata']
      },
      {
        operation: 'transfer',
        conditions: ['from', 'to', 'amount', 'memo']
      },
      {
        operation: 'limit_order',
        conditions: []
      },
      {
        operation: 'fill_order',
        conditions: []
      },
      {
        operation: 'fill_convert_request',
        conditions: []
      },
      {
        operation: 'convert',
        conditions: []
      },
      {
        operation: 'fill_recurrent_transfer',
        conditions: []
      },
      {
        operation: 'request_account_recovery',
        conditions: []
      },
      {
        operation: 'producer_reward',
        conditions: []
      },
      {
        operation: 'author_reward',
        conditions: []
      },
      {
        operation: 'curation_reward',
        conditions: []
      }
    ]
  },
  splinterlands: {
    settings: {
      name: 'Splinterlands',
      prefix: 'sm_',
      color: 'gold'
    },
    operations: [
      {
        operation: 'custom_json',
        conditions: ['required_auths', 'id', 'json', ['to', 'qty', 'token']],
        constants: { id: 'sm_token_transfer' }
      }
    ]
  }
}

export function getNotificationPackages(username: string): any {
  return {
    blogBasics: [
      {
        operation: 'vote',
        conditions: { author: { '==': username } }
      },
      {
        operation: 'comment',
        conditions: { parent_author: { '==': username } }
      }
    ],
    hiveSecurity: [{}],
    test: [
      {
        operation: 'vote',
        conditions: { author: { '==': username } }
      }
    ]
  }
}
